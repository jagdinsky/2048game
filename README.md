# "2048" GAME

## Bash commands for building
sh compile.sh

cd src

cmake CMakeLists.txt

make

cd bin

./2048

## По заданию

На данный момент выполнены пункты задания 1 и 3. Игра работает, логика и анимации написаны, но не готова часть с тестами.